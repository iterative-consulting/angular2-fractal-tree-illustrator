import { Injectable } from '@angular/core';

@Injectable()
export class CanvasService {
  leftBranch: number;

  setLeftBranch(len: number) {
    console.log('setting lest branch');
    console.log(this.leftBranch);
    this.leftBranch = len;
  }

  getLeftBranch(): number {
    console.log('getting left branch');
    console.log(this.leftBranch);
    return this.leftBranch;
  }
}
