/// <reference path="../p5.d.ts" />
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var canvas_service_1 = require("./canvas.service");
var CanvasComponent = (function () {
    function CanvasComponent(canvasService) {
        this.canvasService = canvasService;
        this.height = 800;
        this.width = 800;
    }
    CanvasComponent.prototype.ngOnInit = function () {
        var _this = this;
        var s = function (p) {
            p.setup = function () {
                p.frameRate(10);
                _this.inputDivX = _this.width + 50;
                _this.p = p;
                _this.canvas = p.createCanvas(_this.height, _this.width);
                _this.canvas.position(0, 0);
            };
            p.draw = function () {
                p.background(55);
                p.stroke(255);
                p.translate(_this.width / 2, _this.height);
                _this.branch(220);
            };
        };
        new p5(s);
    };
    CanvasComponent.prototype.branch = function (len) {
        var p = this.p;
        p.line(0, 0, 0, -len);
        p.translate(0, -len);
        if (len > 8) {
            this.drawBranch(this.angle, len * this.leftBranch);
            this.drawBranch(-this.angle, len * this.rightBranch);
        }
    };
    CanvasComponent.prototype.drawBranch = function (angle, len) {
        var p = this.p;
        p.push();
        p.rotate(angle);
        this.branch(len);
        p.pop();
    };
    return CanvasComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], CanvasComponent.prototype, "leftBranch", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], CanvasComponent.prototype, "rightBranch", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], CanvasComponent.prototype, "angle", void 0);
CanvasComponent = __decorate([
    core_1.Component({
        selector: 'canvas',
        template: "",
        providers: [canvas_service_1.CanvasService]
    }),
    __metadata("design:paramtypes", [canvas_service_1.CanvasService])
], CanvasComponent);
exports.CanvasComponent = CanvasComponent;
//# sourceMappingURL=canvas.component.js.map