/// <reference path="../p5.d.ts" />

import { Component, OnInit, Input } from '@angular/core';
import { CanvasService } from './canvas.service';

declare var p5: any;

@Component({
  selector: 'canvas',
  template: ``,
  providers: [CanvasService]
})

export class CanvasComponent implements OnInit {
  p: any;
  canvas: any;
  height: number = 800;
  width: number = 800;
  @Input() leftBranch: number;
  @Input() rightBranch: number;
  @Input() angle: number;

  inputDivX: number;

  constructor(private canvasService: CanvasService) { }

  ngOnInit() {
    const s = (p: any) => {
      p.setup = () => {
        p.frameRate(10);
        this.inputDivX = this.width + 50;
        this.p = p;
        this.canvas = p.createCanvas(this.height, this.width);
        this.canvas.position(0, 0);
      };

      p.draw = () => {
        p.background(55);
        p.stroke(255);
        p.translate(this.width / 2, this.height);
        this.branch(220);
      };
    };

    new p5(s);
  }

  branch(len: number) {
    const p = this.p;
    p.line(0, 0, 0, -len);
    p.translate(0, -len);
    if (len > 8) {
      this.drawBranch(this.angle, len * this.leftBranch);
      this.drawBranch(-this.angle, len * this.rightBranch);
    }
  }

  drawBranch(angle: number, len: number) {
    const p = this.p;
    p.push();
    p.rotate(angle);
    this.branch(len);
    p.pop();
  }
}
