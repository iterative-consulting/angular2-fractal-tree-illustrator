/// <reference path="../p5.d.ts" />

import { Component, OnInit } from '@angular/core';
import { CanvasService } from './canvas.service';
declare var p5: any;

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.html',
  providers: [CanvasService]
})
export class AppComponent implements OnInit {
  leftBranch = .67;
  rightBranch = .67;
  angle = Math.PI / 4;
  pi = Math.PI;

  constructor(private canvasService: CanvasService) { }

  ngOnInit(): void {
  }

  radToDegrees(): number {
    return this.angle * (180 / Math.PI);
  }
}


